package com.dq.kardex.api;

import com.dq.kardex.model.Categoria;
import com.dq.kardex.model.Movimiento;
import com.dq.kardex.model.Producto;
import com.dq.kardex.repository.RCategoria;
import com.dq.kardex.repository.RMovimiento;
import com.dq.kardex.repository.RProducto;
import com.dq.kardex.service.SMovimientos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CMovimientos {

    @Autowired
    RCategoria rCategoria;

    @Autowired
    RProducto rProducto;

    @Autowired
    SMovimientos sMovimientos;

    @CrossOrigin
    @GetMapping(value = "/categorias", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Categoria> getCategorias() {
        return rCategoria.findAll();
    }

    @CrossOrigin
    @GetMapping(value = "/productos", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Producto> getProductos() {
        return rProducto.findAll();
    }

//    @CrossOrigin
//    @GetMapping(value = "/movimientos", produces = MediaType.APPLICATION_JSON_VALUE)
//    public List<Movimiento> getMovimientos() {
//        return rMovimiento.findAll();
//    }

    @CrossOrigin
    @PostMapping(value = "/nuevoMovimiento")
    public ResponseEntity<Movimiento> nuevoMovimiento(@RequestBody Movimiento movimiento){


        Movimiento movres = sMovimientos.nuevoMovimiento(movimiento);
        HttpStatus status = movres!=null?HttpStatus.OK:HttpStatus.BAD_REQUEST;
//        movres.setCantidad(1);
//        movres.setIdMovimiento(33L);
//        movres.setProducto(new Producto());
        return new ResponseEntity<>(movres, status);

    }

}
