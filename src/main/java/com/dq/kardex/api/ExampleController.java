package com.dq.kardex.api;

import com.dq.kardex.model.Categoria;
import com.dq.kardex.repository.RCategoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api2")
public class ExampleController {

    @Autowired
    RCategoria rCategoria;


    @GetMapping(value = "/ej", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> metodoEjemplo() {
        String status = "OK";
        return Arrays.asList(status);
    }

    @PostMapping("/ejPost")
    public List<String> metodoEjemploPost(String id) {
        String nombre = "Diego";
        String apellido = "Quiroga";
        int edad = 34;
        StringBuilder b = new StringBuilder();
        b.append("{")
                .append("nombre:\"" + nombre + "\"")
                .append("apellido:\"" + apellido + "\"")
                .append("edad:" + edad)
                .append("}");
        String cosa = b.toString();

        return Arrays.asList(cosa);
    }

    @CrossOrigin
    @GetMapping(value = "/categorias", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Categoria> getCategorias() {
        return rCategoria.findAll();
    }

}
