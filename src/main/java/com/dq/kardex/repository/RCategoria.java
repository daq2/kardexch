package com.dq.kardex.repository;

import com.dq.kardex.model.Categoria;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RCategoria extends CrudRepository<Categoria, Long> {
    List<Categoria> findAll();
}
