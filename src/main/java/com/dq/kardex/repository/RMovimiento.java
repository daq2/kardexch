package com.dq.kardex.repository;

import com.dq.kardex.model.Movimiento;
import com.dq.kardex.model.Producto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RMovimiento extends CrudRepository<Movimiento, Long>{
    List<Movimiento> findAll();
}
