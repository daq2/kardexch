package com.dq.kardex.repository;

import com.dq.kardex.model.Categoria;
import com.dq.kardex.model.Producto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RProducto extends CrudRepository<Producto, Long>{
    List<Producto> findAll();
}
