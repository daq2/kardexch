package com.dq.kardex.service;

import java.util.List;

import com.dq.kardex.model.Movimiento;
import com.dq.kardex.model.Producto;
import org.springframework.stereotype.Service;

import javax.xml.ws.ServiceMode;

@Service
public interface SMovimientos {
	
	public List<Producto> movimientoStock(List<Producto> p);

	public Movimiento nuevoMovimiento(Movimiento movimiento);
}
