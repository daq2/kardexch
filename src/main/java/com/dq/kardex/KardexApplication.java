package com.dq.kardex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//public class KardexApplication extends SpringServletContainerInitializer{
public class KardexApplication {

    public static void main(String[] args) {
        SpringApplication.run(KardexApplication.class, args);
    }
}
