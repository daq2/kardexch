package com.dq.kardex.test;

import com.dq.kardex.api.ExampleController;
import com.dq.kardex.model.Movimiento;
import com.dq.kardex.repository.RCategoria;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ExampleController.class)
public class MovimientosTest {

    @MockBean
    RCategoria rCategoria;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void ejemploTest() throws Exception {
        String url = "/api2/ej";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(url);

        String resp = "[\"OK\"]";

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        String salida = response.getContentAsString();
        assertThat(salida).isEqualTo(resp);
        assertEquals(HttpStatus.OK.value(), response.getStatus());


    }

    @Test
    public void ejemploNuevoMovimientoVacio() throws Exception {

        String url = "/api/nuevoMovimiento";
        Movimiento movimiento = new Movimiento();
        String jsonMovimiento = new ObjectMapper().writeValueAsString(movimiento);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(url)
                .content(jsonMovimiento)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String stringContent = response.getContentAsString();
        HttpStatus status = HttpStatus.valueOf(response.getStatus());

        assertThat(stringContent).isEmpty();
        assertNotEquals(HttpStatus.OK.value(), response.getStatus());
    }


}
