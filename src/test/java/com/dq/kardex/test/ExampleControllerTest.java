package com.dq.kardex.test;

import com.dq.kardex.api.CMovimientos;
import com.dq.kardex.api.ExampleController;
import com.dq.kardex.model.Movimiento;
import com.dq.kardex.repository.RCategoria;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ExampleController.class)
public class ExampleControllerTest {

    @MockBean
    RCategoria rCategoria;

    @Autowired
    CMovimientos cMovimientos;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void ejemploNuevoMovimientoVacio() throws Exception {
//        CMovimientos cMovimientos = new CMovimientos();

        String url = "/api2/ej";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(url);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        String salida = response.getContentAsString();
        assertThat(salida).isEqualTo("OK");
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        Movimiento movimiento = new Movimiento();

        cMovimientos.nuevoMovimiento(movimiento);

        assertEquals(HttpStatus.BAD_REQUEST, null);
    }


    @Test
    public void ejemploRest() throws Exception {
        String url = "/api2/ej";
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(url);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        String salida = response.getContentAsString();
        assertThat(salida).isEqualTo("OK");
        assertEquals(HttpStatus.OK.value(), response.getStatus());


        //        Assert.assertEquals("OK", exampleController.metodoEjemplo());
    }


}
